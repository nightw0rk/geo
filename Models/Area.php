<?php

/**
 * @property int id
 * @name Area
 * @description Области,республики,края,Автономные округа
 * @version 1.0
 * @author Nightw0rk <nightw0rk@mail.ru>
 * @copyright (c) 2013, Nightw0rk
 * website:	http://www.fraht-center.ru/
 */
class Area extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{areas}}';
    }

    public function relations() {
        return array(
            'Cities' => array(self::HAS_MANY, 'City', 'area_id'),
            'Region' => array(self::BELONGS_TO, 'Region', 'region_id'),
        );
    }

}

?>
