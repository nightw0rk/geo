<?php

/**
 * @property float lat
 * @property float long
 * @property int area_id
 * @property string name
 * @property int id
 * @name City
 * @description Город
 * @version 1.0
 * @author Nightw0rk <nightw0rk@mail.ru>
 * @copyright (c) 2013, Nightw0rk
 * website:    http://www.fraht-center.ru/
 */
class City extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{cities}}';
    }

    public function relations()
    {
        return array(
            'Area' => array(self::BELONGS_TO, 'Area', 'area_id')
        );
    }

    public function getFullAddress()
    {
        return sprintf("%s ФО, %s, %s", $this->Area->Region->name, $this->Area->name, $this->name);
    }

    public function getCoordinates()
    {
        if ($this->long != 0 && $this->lat != null)
            return sprintf("%f,%f", $this->long, $this->lat);
        else
            return Yii::app()->getModule('map')->geo->getCoordinates($this);
    }

    public function getArrayCoordinates()
    {
        return array($this->lat, $this->long);
    }
}

