<?php
/**
 * Description of Country
 *
 * @property string  name
 * @property string short_name
 * @property Integer id
 * @author Администратор
 */
class Country extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{countries}}';
    }

    public function relations() {
        return array(
            'regions' => array(self::BELONGS_TO, 'Region', 'country_id')
        );
    }

}
