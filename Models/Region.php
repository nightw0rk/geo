<?php

/**
 * @name Region
 * @description Федеральный округ
 * @version 1.0
 * @author Nightw0rk <nightw0rk@mail.ru>
 * @copyright (c) 2013, Nightw0rk
 * website:	http://www.fraht-center.ru/
 */
class Region extends CActiveRecord {

    public static function model($className = __CLASS__) {
       return parent::model($className);
    }

    public function tableName() {
        return '{{regions}}';
    }

    public function relations() {
        return array(
            'Areas' => array(self::HAS_MANY, 'Area', 'region_id'),
        );
    }

}

?>
