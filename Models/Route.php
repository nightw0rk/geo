<?php

/**
 * @name Route
 * @description (Ввести описание)
 * @version 1.0
 * @author Nightw0rk <nightw0rk@mail.ru>
 * @copyright (c) 2013, Nightw0rk
 * @property int inPoint
 * @property int outPoint
 * @property float length
 */
class Route extends CActiveRecord
{
    protected $earthRadius = 6371;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'from' => array(self::BELONGS_TO, "City", 'inPoint'),
            'to' => array(self::BELONGS_TO, "City", 'outPoint')
        );
    }

    public function tableName()
    {
        return '{{routes}}';
    }

    /**
     * Найти маршрут
     * @param $from City
     * @param $to City
     * @return Route
     */
    public static function getRoute($from, $to)
    {
        return Route::model()->find('inPoint=:ip and outPoint=:op', array(':ip' => $from->id, ':op' => $to->id));
    }

    /**
     * Получить объект марщрутов
     */
    public static function getJsonRoutes()
    {
        $cities = City::model()->findall();
        $result = array('nodes' => array(), 'links' => array());
        foreach ($cities as $city) {
            $result['nodes'][(int)$city->id] = array(
                'name' => $city->name,
                'group' => $city->area_id,
                'x' => cos(270*pi()/180) * $city->lat - sin(270*pi()/180) * $city->long,
                'y' => sin(270*pi()/180) * $city->lat - cos(270*pi()/180) * $city->long);
        }
        $routes = Route::model()->findAll();
        foreach ($routes as $route) {

            array_push($result['links'], array("source" => $route->inPoint, "target" => $route->outPoint, "value" => ceil($route->length / 1000)));
        }
        return CJSON::encode($result);
    }
}
