-- ������ ������������ Devart dbForge Studio for MySQL, ������ 6.0.128.1
-- �������� �������� ��������: http://www.devart.com/ru/dbforge/mysql/studio
-- ���� �������: 04.06.2013 11:43:53
-- ������ �������: 5.1.69
-- ������ �������: 4.1

USE geo;

DELIMITER $$

CREATE DEFINER = 'root'@'%'
FUNCTION getAngle (fromX float, fromY float, toX float, toY float, tarX float, tarY float)
RETURNS float
BEGIN
  DECLARE vecFrToX float;
  DECLARE vecFrTarX float;
  DECLARE vecFrToY float;
  DECLARE vecFrTarY float;
  DECLARE vecTop float;
  DECLARE vecBottom1 float;
  DECLARE vecBottom2 float;

  SET vecFrToX = toX - fromX;
  SET vecFrTarX = tarX - fromX;
  SET vecFrToY = toY - fromY;
  SET vecFrTarY = tarY - fromY;
  SET vecTop = (vecFrToX * vecFrTarX) + (vecFrToY * vecFrTarY);
  SET vecBottom1 = SQRT(POWER(vecFrToX, 2) + POWER(vecFrToY, 2)); #*
  SET vecBottom2 = SQRT(POW(vecFrTarX, 2) + POW(vecFrTarY, 2));
  #(/   ;END
  RETURN vecTop / (vecBottom1 * vecBottom2);
END
$$

DELIMITER ;