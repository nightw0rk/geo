-- ������ ������������ Devart dbForge Studio for MySQL, ������ 6.0.128.1
-- �������� �������� ��������: http://www.devart.com/ru/dbforge/mysql/studio
-- ���� �������: 04.06.2013 11:44:36
-- ������ �������: 5.1.69
-- ������ �������: 4.1

USE geo;

DELIMITER $$

CREATE DEFINER = 'root'@'%'
FUNCTION getDirection (fromX float, fromY float, toX float, toY float)
RETURNS int(11)
BEGIN
  #left
  IF fromX > toX THEN
    # up
    IF fromY < toY THEN
      RETURN 1;
    END IF;
    #left
    IF fromY > toY THEN
      RETURN 3;
    END IF;
    # only left
    IF fromY = toY THEN
      RETURN 7;
    END IF;
  END IF;
  # right
  IF fromX < toX THEN
    # up
    IF fromY < toY THEN
      RETURN 2;
    END IF;
    #down
    IF fromY > toY THEN
      RETURN 4;
    END IF;
    # only right
    IF fromY = toY THEN
      RETURN 8;
    END IF;
  END IF;
  # up or down
  IF fromX = toX THEN
    # up
    IF fromY < toY THEN
      RETURN 5;
    END IF;
    #down
    IF fromY < toY THEN
      RETURN 6;
    END IF;
    IF fromY = toY THEN
      RETURN 0;
    END IF;
  END IF;
  # nothing
  RETURN - 1;
END
$$

DELIMITER ;