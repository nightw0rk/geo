-- ������ ������������ Devart dbForge Studio for MySQL, ������ 6.0.128.1
-- �������� �������� ��������: http://www.devart.com/ru/dbforge/mysql/studio
-- ���� �������: 04.06.2013 11:46:39
-- ������ �������: 5.1.69
-- ������ �������: 4.1

USE geo;

CREATE TABLE cachetable (
  id int(10) UNSIGNED NOT NULL,
  value int(11) DEFAULT NULL,
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
AVG_ROW_LENGTH = 297
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE regions (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  short_name varchar(255) DEFAULT NULL,
  country_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_regions_countrys_id FOREIGN KEY (country_id)
  REFERENCES countries (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 9
AVG_ROW_LENGTH = 2048
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE areas (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  short_name varchar(255) DEFAULT NULL,
  region_id int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_areas_region_id FOREIGN KEY (region_id)
  REFERENCES regions (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 76
AVG_ROW_LENGTH = 221
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE cities (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  area_id int(11) UNSIGNED NOT NULL,
  `long` float DEFAULT NULL,
  lat float DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_cities_areas_id FOREIGN KEY (area_id)
  REFERENCES areas (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 4402
AVG_ROW_LENGTH = 65
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE routes (
  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  inPoint int(11) UNSIGNED NOT NULL,
  outPoint int(11) UNSIGNED NOT NULL,
  length float NOT NULL DEFAULT 1,
  PRIMARY KEY (id),
  CONSTRAINT FK_routes_cities_id FOREIGN KEY (inPoint)
  REFERENCES cities (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_routes_cities_id_out FOREIGN KEY (outPoint)
  REFERENCES cities (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 11493
AVG_ROW_LENGTH = 176
CHARACTER SET utf8
COLLATE utf8_general_ci;