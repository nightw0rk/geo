<?php

/**
 * Description of GeoAutocomplete
 * Координаты Долгота;Широта
 * -/-/-/-/-/ Longitude;Latitude
 * @author Nightw0rk
 */
class GeoAutocomplete extends CApplicationComponent
{

    protected $geoUrl = 'http://geocode-maps.yandex.ru/1.x/';
    protected $format = 'json';
    protected $kind = 'locality';
    protected $earthRadius = 6371;
    protected $stepRadiusScan = 5;
    // ROUTE DIR
    const LU_DIR = 1;
    const RU_DIR = 2;
    const LD_DIR = 3;
    const RD_DIR = 4;
    const U_DIR = 5;
    const D_DIR = 6;
    const L_DIR = 7;
    const R_DIR = 8;


    /**
     * Поиск объектов по запросу
     * @param string $searchQuery строка поиска
     * @param int $limit кол-во результатов
     * @return array список найденых объектов
     */
    public function search($searchQuery, $limit = -1)
    {
        // Запрос из кеша
        $hash = hash('md5', $searchQuery . $limit);
        $result = Yii::app()->cache->get($hash);
        if ($result === false) {
            $query = new CDbCriteria();
            $query->addSearchCondition('name', $searchQuery);
            $query->limit = $limit;
            $searchResult = City::model()->findAll($query);
            /** @var $value City */
            foreach ($searchResult as $value) {
                $result[intval($value->id)] = array('name' => $value->name,
                    'full_name' => $value->getFullAddress());
            }
            $webResult = (object)$this->makeRequest($this->geoUrl, array('query' => array(
                'format' => $this->format,
                'kind' => $this->kind,
                'geocode' => $searchQuery,
                'results' => $limit == -1 ? 10 : $limit
            )));
            if ($webResult->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found >= 0) {
                foreach ($webResult->response->GeoObjectCollection->featureMember as $geoObject) {
                    if ($geoObject->GeoObject->metaDataProperty->GeocoderMetaData->kind == $this->kind) {
                        $obj = $geoObject->GeoObject->metaDataProperty->GeocoderMetaData;
                        /** @var $country Country */
                        $country = Country::model()->find('name = :name', array(':name' => $obj->AddressDetails->Country->CountryName));
                        if ($country == NULL) {
                            $country = new Country();
                            $country->name = $obj->AddressDetails->Country->CountryName;
                            $country->short_name = $obj->AddressDetails->Country->CountryNameCode;
                            $country->save();
                        }
                        /** @var $area Area */
                        $area = Area::model()->find('name=:name', array(':name' => $obj->AddressDetails->Country->AdministrativeArea->AdministrativeAreaName));
                        if ($area != NULL) {
                            if (method_exists($obj->AddressDetails->Country->AdministrativeArea, 'Locality')) {
                                $city_name = $obj->AddressDetails->Country->AdministrativeArea->Locality->LocalityName;
                            } else {
                                try {
                                    $city_name = @$obj->AddressDetails->Country->AdministrativeArea->SubAdministrativeArea->Locality->LocalityName;
                                } catch (Exception $e) {
                                    Yii::trace($e->getMessage());
                                    $city_name = "";
                                }
                            }
                            if ($city_name != "") {
                                $city = City::model()->find('name=:name and area_id = :ida', array(':name' => $city_name, ':ida' => $area->id));
                                if ($city == NULL) {
                                    $pos = explode(" ", $geoObject->GeoObject->Point->pos);
                                    $city = new City();
                                    $city->name = $city_name;
                                    $city->area_id = $area->id;
                                    $city->long = $pos[1];
                                    $city->lat = $pos[0];
                                    $city->save();
                                }
                                $result[$city->id] = array('name' => $city->name,
                                    'full_name' => $city->getFullAddress());
                            }
                        }
                    }
                }
            }
            if ($result !== false) {
                //если что то нашли
                $result = json_encode($result, JSON_FORCE_OBJECT);
                Yii::app()->cache->set($hash, $result);
            }
        }
        return $result;
    }

    /**
     * @param string $response
     * @return stdClass
     * @throws Exception
     */
    protected function parseJson($response)
    {
        try {
            $result = json_decode($response);
            $error = $this->fetchJsonError($result);
            if (!isset($result)) {
                throw new Exception(Yii::t('eauth', 'Invalid response format.', array()), 500);
            } else if (isset($error) && !empty($error['message'])) {
                throw new Exception($error['message'], $error['code']);
            } else
                return $result;
        } catch (Exception $e) {
            print($response);
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Returns the error info from json.
     * @param stdClass $json the json response.
     * @return array the error array with 2 keys: code and message. Should be null if no errors.
     */
    protected function fetchJsonError($json)
    {
        if (isset($json->error)) {
            return array(
                'code' => 500,
                'message' => 'Unknown error occurred.',
            );
        } else
            return null;
    }

    /**
     * Makes the curl request to the url.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return mixed
     * @throws Exception
     */
    protected function makeRequest($url, $options = array(), $parseJson = true)
    {
        $ch = $this->initRequest($url, $options);

        $result = curl_exec($ch);
        $headers = curl_getinfo($ch);

        if (curl_errno($ch) > 0)
            throw new Exception(curl_error($ch), curl_errno($ch));

        if ($headers['http_code'] == 500) {
            print("!E сервер вернул ошибку....\n");
            for ($i = 0; $i < 5; $i++) {
                print("!I попытка связи " . $i . "....\n");
                $result = curl_exec($ch);
                $headers = curl_getinfo($ch);
                if ($headers['http_code'] == 200) break;
                sleep(1);
            }
        }
        if ($headers['http_code'] != 200) {
            Yii::log(
                'Invalid response http code: ' . $headers['http_code'] . '.' . PHP_EOL .
                    'URL: ' . $url . PHP_EOL .
                    'Options: ' . var_export($options, true) . PHP_EOL .
                    'Result: ' . $result, CLogger::LEVEL_ERROR, 'application.extensions.eauth'
            );
            //var_dump($headers);
            //throw new Exception(Yii::t('eauth', 'Invalid response http code: {code}.', array('{code}' => $headers['http_code'])), $headers['http_code']);
            return null;
        }

        curl_close($ch);

        if ($parseJson)
            $result = $this->parseJson($result);

        return $result;
    }

    /**
     * Prepare curl request
     * @param $url string
     * @param $options array
     * @return resource curl
     */
    protected function initRequest($url, $options = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        if (defined('PROXY')) {
            curl_setopt($ch, CURLOPT_PROXY, '192.168.0.253:3128');
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, "pronin:5246389");
        }
        if (isset($options['referer']))
            curl_setopt($ch, CURLOPT_REFERER, $options['referer']);

        if (isset($options['headers']))
            curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);

        if (isset($options['query'])) {
            $url_parts = parse_url($url);
            if (isset($url_parts['query'])) {
                $query = $url_parts['query'];
                if (strlen($query) > 0)
                    $query .= '&';
                $query .= http_build_query($options['query']);
                $url = str_replace($url_parts['query'], $query, $url);
            } else {
                $url_parts['query'] = $options['query'];
                $new_query = http_build_query($url_parts['query']);
                $url .= '?' . $new_query;
            }
        }

        if (isset($options['data'])) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $options['data']);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        return $ch;
    }

    /**
     * Получить координаты по экземпляру городу
     * @param $city City
     * @return string
     */
    public function getCoordinates($city)
    {
        if ($city != NULL) {
            $webResult = (object)$this->makeRequest($this->geoUrl, array('query' => array(
                'format' => $this->format,
                'kind' => $this->kind,
                'geocode' => $city->name,
                'results' => 3
            )));
            if ($webResult->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found >= 0) {
                foreach ($webResult->response->GeoObjectCollection->featureMember as $geoObject) {
                    if ($geoObject->GeoObject->metaDataProperty->GeocoderMetaData->kind == $this->kind) {
                        $pos = explode(" ", $geoObject->GeoObject->Point->pos);
                        $city->long = $pos[1];
                        $city->lat = $pos[0];
                        $city->save();
                        return sprintf("%f,%f", $city->long, $city->lat);
                        break;
                    }
                }
            }
        }
        return "";
    }

    /**
     * Перевести в координаты
     * @param float $value
     * @return float
     */
    public function toRad($value)
    {
        return $value * pi() / 180;
    }

    /**
     * Перевести в градусы
     * @param float $value
     * @return float
     */
    public function toDeg($value)
    {
        return $value * 180 / pi();
    }

    /**
     * Получить точку
     * @param City $point  - Начальная точка
     * @param float $dst - растояние
     * @param float $bearing - азимут
     * @return array
     */
    public function getPoint($point, $dst, $bearing)
    {
        /*
         * φ2 = asin( sin(φ1)*cos(d/R) + cos(φ1)*sin(d/R)*cos(θ) )
         * λ2 = λ1 + atan2( sin(θ)*sin(d/R)*cos(φ1), cos(d/R)−sin(φ1)*sin(φ2) )
         *
         */
        $bearing = $this->toRad($bearing);
        $dst = $dst / $this->earthRadius;
        $lat = $this->toRad($point->long);
        $lon = $this->toRad($point->lat);
        $dot_lat = asin(sin($lat) * cos($dst) + cos($lat) * sin($dst) * cos($bearing));
        $dot_lon = $lon + atan2(sin($bearing) * sin($dst) * cos($lat), cos($dst) - sin($lat) * sin($dot_lat));
        return array($this->toDeg($dot_lat), $this->toDeg($dot_lon));
    }

    /**
     * Получить города с точки
     * @param array(float,float) $coord - координаты поиска
     * @param float $minSquare - минимальная площадь города
     * @return bool|mixed
     */
    function getCity($coord, $minSquare = 0.005)
    {
        $uid = hash('md5', $coord[0] . $coord[1]);
        $result = Yii::app()->cache->get($uid);
        if ($result === false) {
            $cities = (object)$this->makeRequest($this->geoUrl, array('query' => array(
                'format' => $this->format,
                'kind' => $this->kind,
                'spn' => '0.048,0.06',
                'sco' => 'longlat',
                'geocode' => sprintf("%f,%f", $coord[0], $coord[1])
            )));
            if (isset($cities->response)) {
                if ($cities->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found >= 0) {
                    //printf("coord=[%3.6f,%3.6f],f=%d\n", $coord[0], $coord[1], $cities->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found);
                    print '.';
                    foreach ($cities->response->GeoObjectCollection->featureMember as $geoObject) {
                        $lc = explode(" ", $geoObject->GeoObject->boundedBy->Envelope->lowerCorner);
                        $uc = explode(" ", $geoObject->GeoObject->boundedBy->Envelope->upperCorner);
                        $sq = abs($uc[0] - $lc[0]) * abs($uc[1] - $lc[1]);
                        print("=Найден объект " . $geoObject->GeoObject->name . " площ = " . $sq);
                        if ($sq > $minSquare) {
                            $pos = explode(" ", $geoObject->GeoObject->Point->pos);
                            $adr = $geoObject->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails;
                            /** @var $area Area */
                            if (property_exists($adr->Country, "AdministrativeArea")) {
                                $area = Area::model()->find('name=:name', array('name' => $adr->Country->AdministrativeArea->AdministrativeAreaName));
                                if ($area != null) {
                                    /** @var $city City */
                                    $city = City::model()->find('name=:name and area_id=:id', array('name' => $geoObject->GeoObject->name, 'id' => $area->id));
                                    if (!isset($city)) {
                                        $city = new City();
                                        $city->name = $geoObject->GeoObject->name;
                                        $city->area_id = $area->id;
                                        $city->long = $pos[0];
                                        $city->lat = $pos[1];
                                        $city->save();
                                    }
                                    $result[hash('md5', $city->id)] = array('city' => $city);
                                    print "->Принят\n";
                                } else {
                                    print " -> Не найдена область\n";
                                }
                            } else {
                                print "->Не правильный ответ сервера\n";
                            }
                        } else {
                            print "->Отброшен \n";
                        }
                    }
                }
            }
            Yii::app()->cache->set($uid, $result);
        }
        return $result;
    }

    /**
     * @param array $sector
     * @param int $round
     * @param City $center
     * @return array|mixed|null
     */
    public
    function getCitiesFormSector($sector, $round, $center)
    {
        //Yii::log(s
        $id = hash('md5', $sector[0] . $sector[1] . $round . $center->long . $center->lat);
        $cache = Yii::app()->cache->get($id);
        if ($cache === false) {
            printf("s[%2.2f,%2.2f] | r[%d]\n", $sector[0], $sector[1], $round); //);
            $dst = $round * $this->stepRadiusScan;
            $step = ($sector[1] - $sector[0]) / $dst;
            $cities = null;
            for ($x = $sector[0]; $x <= $sector[1]; $x = $x + $step) {
                $point = $this->getPoint($center, $dst, $x);
                $dryCities = $this->getCity($point);
                if (is_array($dryCities)) {
                    foreach ($dryCities as $id => $city) {
                        $lat = (float)($city['city']->lat);

                        $long = (float)($city['city']->long);
                        if (!isset($cities[$id]) && abs($lat - $center->lat) > 0.0001 && abs($long - $center->long) > 0.0001) {
                            $city = array_merge((array)$city, array('distance' => $dst));
                            $cities = array_merge((array)$cities, array($id => $city));
                        }
                    }
                }
            }
            if ($cities == null and $round < 50) {
                $cities = $this->getCitiesFormSector($sector, $round + 1, $center);
            }
            print "\n";
            Yii::app()->cache->set($id, $cities);
        } else {
            $cities = $cache;
        }
        return $cities;
    }

    /**
     * Построить города
     * @param City $from
     * @param City $to
     * @return bool|Route
     */
    public
    function getRoute($from, $to)
    {
        //$id = hash('md5', $from->id . '-' . $to->id);
        //$result = Yii::app()->cache->get($id);
        //if ($result === false) {
        /** @var $route  Route */

        $route = Route::model()->find('inPoint=:iid and outPoint=:oid', array(':iid' => $from->id, ':oid' => $to->id));
        if ($route == NULL) {
            $routeMap = (object)$this->makeRequest('http://maps.yandex.ru/', array('query' => array(
                'output' => $this->format,
                'rtext' => sprintf('%3.5f,%3.5f~%3.5f,%3.5f', $from->lat, $from->long, $to->lat, $to->long)
            )));

            try {
                if (count($routeMap->vpage->data->response->data->features) >= 1) {
                    $route = new Route();
                    $route->inPoint = $from->id;
                    $route->outPoint = $to->id;
                    $route->length = $routeMap->vpage->data->response->data->features[0]->properties->RouteMetaData->Distance->value;
                    $route->save();
                } else {
                    print "!!!маршрут не найден\n";
                }
            } catch (Exception $e) {
                printf("Catch exception %s - %s at line %d", $e->getCode(), $e->getMessage(), $e->getLine());
                echo  "==================================================================";
                var_dump($routeMap);

            }
        }
        //   Yii::app()->cache->set($id, $route);
        $result = $route;
        //}
        return $result;
    }

    /**
     * Генерация графа городов
     * @param City|null $center точка центра
     * @param City|null $beforeCenter передыдуший центр
     * @param boolean $skipCheck
     */
    public
    function generateMapGraph($center, $beforeCenter = null, $skipCheck = false)
    {
        if ($center == null) {
            //                             Long        Lat
            CacheTable::freeCache();
            $center = City::model()->findByPk(1811);
        }
        if ($center !== NULL) {
            $count = CacheTable::getCount($center->id);
            $count = !$skipCheck ? $count : 0;
            if ($count == 0) {
                echo 'Текущий центр поиска - ' . $center->name . "\n";
                $s1 = $this->getCitiesFormSector(array(0, 90), 1, $center);
                $s2 = $this->getCitiesFormSector(array(90, 180), 1, $center);
                $s3 = $this->getCitiesFormSector(array(180, 270), 1, $center);
                $s4 = $this->getCitiesFormSector(array(270, 360), 1, $center);
                $aroundCities = array_merge((array)$s1, (array)$s2, (array)$s3, (array)$s4);
                /** @noinspection PhpUnusedLocalVariableInspection */
                foreach ($aroundCities as $id => $city) {
                    if (isset($city['city'])) {
                        echo 'Создание маршрута: ' . $center->name . ' -> ' . $city['city']->name . "\n";
                        $this->getRoute($center, $city['city']);
                    }
                }
                CacheTable::setCount($center->id, $count + 1);
                /** @noinspection PhpUnusedLocalVariableInspection */
                foreach ($aroundCities as $id => $city) {

                    if (isset($city['city'])) {
                        if ($beforeCenter != null && $city['city']->id != $beforeCenter->id)
                            $this->generateMapGraph($city['city'], $center);
                        elseif ($beforeCenter == null)
                            $this->generateMapGraph($city['city'], $center);
                    }
                }
            }
        }
    }

    /**
     * Получить напрвление маршрута
     * @param array(float,float) $from точка начала
     * @param array(float,float) $to точка конца пути
     * @return int
     */
    public static
    function getRouteDirection($from, $to)
    {
        // left
        if ($from[0] > $to[0]) {
            // up
            if ($from[1] < $to[1])
                return GeoAutocomplete::LU_DIR;
            //down
            if ($from[1] > $to[1])
                return GeoAutocomplete::LD_DIR;
            // only left
            if ($from[1] == $to[1])
                return GeoAutocomplete::L_DIR;
        }
        // right
        if ($from[0] < $to[0]) {
            // up
            if ($from[1] < $to[1])
                return GeoAutocomplete::RU_DIR;
            //down
            if ($from[1] > $to[1])
                return GeoAutocomplete::RD_DIR;
            // only left
            if ($from[1] == $to[1])
                return GeoAutocomplete::R_DIR;
        }
        // up or down
        if ($from[0] == $to[0]) {
            // up
            if ($from[1] < $to[1])
                return GeoAutocomplete::U_DIR;
            //down
            if ($from[1] > $to[1])
                return GeoAutocomplete::D_DIR;
            // nothing
            return -1;
        }
        // nothing
        return -1;
    }

    /**
     * получить список городоа в квандранте
     * @param array(float,float) $from
     * @param array(float,float) $to
     * @return int[]
     */
    public
    function getRoutedCities($from, $to)
    {
        $DirectionRoute = GeoAutocomplete::getRouteDirection($from, $to);
        print $DirectionRoute;
        if ($DirectionRoute > -1) {
            $criteria = new CDbCriteria();
            switch ($DirectionRoute) {
                case GeoAutocomplete::LU_DIR:
                    $criteria->addBetweenCondition('lat', $to[0], $from[0]);
                    $criteria->addBetweenCondition('`long`', $from[1], $to[1]);
                    break;
                case GeoAutocomplete::LD_DIR:
                    $criteria->addBetweenCondition('lat', $to[0], $from[0]);
                    $criteria->addBetweenCondition('`long`', $to[1], $from[1]);
                    break;
                case GeoAutocomplete::RU_DIR:
                    /*
                    $criteria->addBetweenCondition('lat', $from[0], $to[0]);
                    $criteria->addBetweenCondition('`long`', $from[1], $to[1]);
                    */
                    $criteria->addCondition('lat>' . $from[0] . ' and lat<' . $to[0]);
                    $criteria->addCondition('`long`>' . $from[1] . ' and `long`<' . $to[1]);
                    break;
                case GeoAutocomplete::RD_DIR:
                    $criteria->addCondition('lat>' . $from[0] . ' and lat<' . $to[0]);
                    $criteria->addCondition('`long`>' . $to[1] . ' and `long`<' . $from[1]);
                    break;
                case GeoAutocomplete::R_DIR :
                    $QuadrantHeight = ($to[0] - $from[0]) / 2;
                    $criteria->addBetweenCondition('lat', $from[0], $to[0]);
                    $criteria->addBetweenCondition('`long`', $from[1] + $QuadrantHeight / 2, $from[1] - $QuadrantHeight / 2);
                    break;
                case GeoAutocomplete::L_DIR :
                    $QuadrantHeight = ($from[0] - $to[0]) / 2;
                    $criteria->addBetweenCondition('lat', $to[0], $from[0]);
                    $criteria->addBetweenCondition('`long`', $to[1] + $QuadrantHeight / 2, $to[1] - $QuadrantHeight / 2);
                    break;
                case GeoAutocomplete::D_DIR :
                    $QuadrantWidth = ($from[1] - $to[1]) / 2;
                    $criteria->addBetweenCondition('lat', $from[0] - $QuadrantWidth / 2, $from[0] + $QuadrantWidth / 2);
                    $criteria->addBetweenCondition('`long`', $to[1], $from[1]);
                    break;
                case GeoAutocomplete::U_DIR:
                    $QuadrantWidth = ($to[1] - $from[1]) / 2;
                    $criteria->addBetweenCondition('lat', $from[0] - $QuadrantWidth / 2, $from[0] + $QuadrantWidth / 2);
                    $criteria->addBetweenCondition('`long`', $from[1], $to[1]);
                    break;
            }
            var_dump($criteria);
            $cities = City::model()->findAll($criteria);
            $result = array();
            foreach ($cities as $city) {
                array_push($result, $city->id);
            }
            return $result;
        }
    }

    private function deleteFromArray(&$array, $deleteIt, $useOldKeys = FALSE)
    {
        $tmpArray = array();
        $found = FALSE;
        foreach ($array as $key => $value) {
            if ($value !== $deleteIt) {
                if (FALSE === $useOldKeys) {
                    $tmpArray[] = $value;
                } else {
                    $tmpArray[$key] = $value;
                }
            } else {
                $found = TRUE;
            }
        }

        $array = $tmpArray;

        return $found;
    }

    /**
     * @param City $from
     * @param City $to
     * @return int[]
     */
    function getRouteRoundCity($from, $to)
    {
        $point = array($from->lat + ($to->lat - $from->lat) / 2, $from->long + ($to->long - $from->long) / 2);
        $Radius = sqrt(pow($to->lat - $from->lat, 2) + pow($to->long - $from->long, 2)) / 2;
        $criteria = new CDbCriteria();
        $criteria->addCondition('POWER(lat-' . $point[0] . ',2)+POWER(`long`-' . $point[1] . ',2) < ' . pow($Radius, 2));
        $cities = City::model()->findAll($criteria);
        $result = array();
        foreach ($cities as $city) {
            array_push($result, $city->id);
        }
        //var_dump($result);
        return $result;
    }

    /**
     * @param City $fromCity
     * @param City $toCity
     * @return int[];
     */
    public function getRouteWithInlcudeCities($fromCity, $toCity)
    {
        $includedCities = $this->getRouteRoundCity($fromCity, $toCity);
        $pre = $this->getRouteDirection($fromCity->getArrayCoordinates(), $toCity->getArrayCoordinates());
        array_push($includedCities, $toCity->id);
        $theEndOfWay = false;
        $currentCity = $fromCity;
        $result = array($fromCity->name);
        $count = 0;
        $fullLength = 0;
        while (!$theEndOfWay) {
            $routeCriteria = new CDbCriteria();
            $routeCriteria->addCondition('inPoint=' . $currentCity->id);
            $routeCriteria->addInCondition('outPoint', $includedCities);
            $routeCriteria->join = "LEFT JOIN cities ip
                ON inPoint = ip.id
                LEFT JOIN cities op
                ON outPoint = op.id";
            $routeCriteria->addCondition("getAngle(ip.lat,ip.`long`,op.lat,op.`long`," . ($toCity->lat) . "," . $toCity->long . ") > 0  ");
            $routeCriteria->order = "ABS((getAngle(ip.lat,ip.`long`,op.lat,op.`long`," . ($toCity->lat) . "," . $toCity->long . ")/PI()*180) - 90)";
            $route = Route::model()->find($routeCriteria);
            if ($route != null) {
                $result[$count++] = array(
                    'id' => $route->to->id,
                    'name' => $route->to->name,
                    'length' => $route->length,
                    'coords' => array(
                        'lat' => $route->to->lat,
                        'long' => $route->to->long
                    )
                );
                $fullLength += $route->length;
                $this->deleteFromArray($includedCities, $route->outPoint, True);
                $currentCity = $route->to;
                if ($currentCity == $toCity)
                    $theEndOfWay = true;
            } else {
                print "Маршрут не найден !!!!";
                $theEndOfWay = true;


            }
        }
        $result['length']=$fullLength;
        return $result;
    }
}